<?php

namespace Deployer;

require_once 'recipe/common.php';

set('application', 'Shopware 6');
set('allow_anonymous_stats', false);
set('default_timeout', 3600); // Increase the `default_timeout`, if needed when tasks take longer than the limit.

// For more information, please visit the Deployer docs: https://deployer.org/docs/configuration.html
// SSH-HOSTNAME should be replaced with an IP address 
host('tapas.elio-dev.com')
    ->setLabels([
        'type' => 'web',
        'env'  => 'prod',
    ])
    ->setRemoteUser('www-data')
    ->set('deploy_path', '/var/www/html/shopware')
    ->set('http_user', 'www-data') // Not needed, if the `user` is the same user, the webserver is running with
    ->set('writable_mode', 'chmod')
    ->set('keep_releases', 3); // Keeps 3 old releases for rollbacks (if no DB migrations were executed) 

// For more information, please visit the Deployer docs: https://deployer.org/docs/configuration.html#shared_files
set('shared_files', [
    '.env',
]);

// This task uploads the whole workspace to the target server
task('deploy:update_code', static function () {
    upload('.', '{{release_path}}');
});

//Debug task to check configuration

task('deploy:check_config', function () {
    writeln('Shared Files: ' . print_r(get('shared_files'), true));
});

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:check_config',
    'deploy:update_code',
    'deploy:publish',
])->desc('Deploy your project');

after('deploy:failed', 'deploy:unlock');
